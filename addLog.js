const fs = require("fs")

// middleware to append request information to log file
const appendLog = function (req, res, next) {
    const date = new Date()

    const data = {
        reqUrl: `${req.protocol}://${req.get("host")}${req.originalUrl}`,
        method: req.method,
        currDate: `${date.getDate().toString()}/${date
            .getMonth()
            .toString()}/${date.getFullYear().toString()}`,
    }

    fs.appendFile("./logs.log", `${JSON.stringify(data)}\n`, "utf-8", (err) => {
        if (err) {
            console.log(err.message)
        } else {
            console.log("data successfully added")
        }
    })

    next()
}

module.exports = appendLog
