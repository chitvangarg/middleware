const express = require("express")
const { port } = require("./config")
const appendRequest = require("./addLog")
const uuid = require("uuid4")

const app = express()

// Server Creation
app.listen(port, () => {
    console.log(`Server started successfully at ${port}`)
})

// Middleware to parse json content
app.use(express.json())

app.get("/log", (req, res) => {
    res.sendFile(`${__dirname}/logs.log`)
})

// use of global middleware
app.use(appendRequest)

app.get("/html", (req, res) => {
    res.send("<h1>Welcome to mountblue</h1>")
})

app.post("/json", (req, res) => {
    const value = { ...req.body, city: "Ghaziabad" }
    res.json(value)
})

app.get("/uuid", (req, res) => {
    const uniqueId = uuid()
    res.send(`The unique Id is: ${uniqueId}`)
})
